%==========================================================================
% Author:           Brandon Ferreira
% Student Number:   FRRBRA002 - University of Cape Town
% Title:            MutualMasking
%
% Description:
%
% Simulates the effects of mutual masking for different algorithms
%
% Notes:
%   - Currently implemented algorithms include CA, GOCA, SOCA, and OS-CFAR
%
%==========================================================================
close all; clear all;

DataSize = 100;

Data = randn(1,DataSize) + 1i*randn(1,DataSize);
Data = abs(Data).^2;
Data(44:47) = 30;
Data(50:53) = 100;
%==========================================================================
% Testing Variables
%==========================================================================
RefWindow = 32;
guardCells = 1;
TargetPFA = 10^-3;
%==========================================================================
aCA = getConstant("CA",RefWindow,guardCells,TargetPFA);
aGO = getConstant("GOCA",RefWindow,guardCells,TargetPFA);
aSO = getConstant("SOCA",RefWindow,guardCells,TargetPFA);
aOS = getConstant("OS",RefWindow,guardCells,TargetPFA);

TCA = getThreshold("CA",Data,RefWindow,guardCells,aCA);
TGO = getThreshold("GOCA",Data,RefWindow,guardCells,aGO);
TSO = getThreshold("SOCA",Data,RefWindow,guardCells,aSO);
TOS = getThreshold("OS",Data,RefWindow,guardCells,aOS);
%==========================================================================
% Displaying Data
%==========================================================================
figure();

plot(1:DataSize,20*log10(Data),1:DataSize,20*log10(TCA),1:DataSize,20*log10(TGO),1:DataSize,20*log10(TSO),1:DataSize,20*log10(TOS));
title("Noise and Data Thresholds"); 
xlabel("Samples"); 
ylabel("Magnitude");
legend("Noise","CA-CFAR","GOCA-CFAR","SOCA-CFAR","OS-CFAR");
%==========================================================================
% END
%==========================================================================
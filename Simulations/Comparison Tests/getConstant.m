%==========================================================================
% Author:           Brandon Ferreira
% Student Number:   FRRBRA002 - University of Cape Town
% Title:            getConstant
%
% Description:
%
% getConstant will return the CFAR constant of an algorithm specified
% through given input parameters
%
% Notes:
%   - Currently implemented algorithms include CA, GOCA, SOCA, and OS-CFAR
%
%==========================================================================
function [CFAR_CONSTANT] = getConstant(Algorithm,RefWindow,guardCells,TargetPFA)
    %======================================================================
    % Parameter Calculation
    %======================================================================
    N = RefWindow-(2*guardCells);
    %======================================================================
    % Algorithm Selection
    %======================================================================
    if (Algorithm == "GOCA")
        aGO = 0.75;
        factor = 0.01;
        x = 0;
        flag = 2;

        while(x<300)
            var1 = (1 + aGO)^(-N/2);
            var2 = (2 + aGO)^(-N/2);
            var3 = 0;

            for k = 0:(N/2 - 1)
                %==========================================================
                % Factorial Parameters
                %==========================================================
                m = N/2 - 1 + k;
                n = k;
                %==========================================================
                % Sumation Parameters
                firstHalf = factorial(m)/(factorial(m-n)*factorial(n));
                secondHalf = (2 + aGO)^(-k);
                %==========================================================
                var3 = var3 + (firstHalf)*(secondHalf);
            end
            PFA_GO = 2*(var1 - var2*var3);
            %display([PFA_GO,aGO]);

            if(PFA_GO < TargetPFA)
                if (flag == 0)
                    factor = factor - 0.001;
                end
                aGO = aGO - factor;
                flag = 1;
            elseif (round(PFA_GO,4) == TargetPFA)
                break;
            else
                if (flag == 1)
                   factor = factor - 0.001; 
                end
                aGO = aGO + factor;
                flag = 0;
            end
            x = x + 1;
        end
        CFAR_CONSTANT = aGO;
    %======================================================================    
    elseif (Algorithm == "SOCA")
        aSO = 1;
        factor = 0.01;
        x = 0;
        flag = 2;

        while(x<300)
            var1 = (2 + aSO)^(-N/2);
            var2 = 0;

            for k = 0:(N/2 - 1)
                %==========================================================
                % Factorial Parameters
                %==========================================================
                m = N/2 - 1 + k;
                n = k;
                %==========================================================
                % Sumation Parameters
                %==========================================================
                firstHalf = factorial(m)/(factorial(m-n)*factorial(n));
                secondHalf = (2 + aSO)^(-k);
                %==========================================================
                var2 = var2 + (firstHalf)*(secondHalf);
            end

            PFA_SO = 2*var1*var2;
            %display([PFA_SO,aSO,factor])

            if(PFA_SO < TargetPFA)
                if (flag == 0)
                    factor = factor - 0.001;
                end
                aSO = aSO - factor;
                flag = 1;
            elseif (round(PFA_SO,4) == TargetPFA)
                break;
            else
                if (flag == 1)
                   factor = factor - 0.001; 
                end
                aSO = aSO + factor;
                flag = 0;
            end
            x = x + 1; 
        end     
        CFAR_CONSTANT = aSO;
    %======================================================================
    elseif (Algorithm == "CA")
        aCA = ((TargetPFA)^(-1/N)) - 1;
        CFAR_CONSTANT = aCA;
    %======================================================================
    elseif (Algorithm == "OS")
        aOS = 10;
        factor = 0.1;
        flag = 2;

        x = 0;
        while(x<300)

            %k = N/2 + (0);       %  Adjust k by changing number in brakets
            k = round((0.75)*N);
            i=0;
            PFA_OS = (N-i)/(N-i+aOS);
            for i = 1:(k-1)
                PFA_OS = PFA_OS*((N-i)/(N-i+aOS));
            end
            %display([PFA_OS,aOS,factor])

            if(PFA_OS < TargetPFA)
                if (flag == 0)
                    factor = factor - 0.01;
                end
                aOS = aOS - factor;
                flag = 1;
            elseif (round(PFA_OS,4) == TargetPFA)
                targetA = aOS;
            else
                if (flag == 1)
                   factor = factor - 0.01; 
                end
                aOS = aOS + factor;
                flag = 0;
            end
            x = x + 1; 
        end     
        CFAR_CONSTANT = aOS;
    %======================================================================
    else
        CFAR_CONSTANT = 0;
    end
    %======================================================================
    
end
%==========================================================================
% End
%==========================================================================
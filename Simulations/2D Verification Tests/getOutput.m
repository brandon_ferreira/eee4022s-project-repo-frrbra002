%==========================================================================
% Author:           Brandon Ferreira
% Student Number:   FRRBRA002 - University of Cape Town
% Title:            getOutput
%
% Description:
%
% getOutput accepts a data matrix (i.e: RTI map) and applies a threshold
% generated for that data
%
% Notes:
%   - Makes use of a overlap factor of 50%
%
%==========================================================================
function [Output,Detections,xDetect,yDetect] = getOutput(Data,Threshold)

    xlen = size(Data,2);
    ylen = size(Data,1);
    
    Detections = 0;
    Output = Data;
    xDetect = []; yDetect = [];
    detectIndex = 1;
    
    for y = 1:ylen

        for x = 1:xlen
            if (Threshold(y,x)>Data(y,x))
               Output(y,x) = 0;
            else
               Detections = Detections + 1;
               Output(y,x) = Output(y,x) + 20000;
               xDetect(detectIndex) = x;
               yDetect(detectIndex) = y;
               detectIndex = detectIndex + 1;
            end
        end

    end
end
%==========================================================================
% END
%==========================================================================
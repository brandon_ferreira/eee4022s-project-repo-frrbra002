%==========================================================================
% Author:           Brandon Ferreira
% Student Number:   FRRBRA002 - University of Cape Town
% Title:            getNoiseSamples
%
% Description:
%
% Generates noise samples and applies thresholding techniques on all.
% Returns the thresholded data as well as originally generated noise.
%
% Notes:
%
%==========================================================================
function [Noise,CA,DCA,TCA,GOCA,DGO,TGO,SOCA,DSO,TSO,OS,DOS,TOS] = getNoiseSamples(N,RefWindow,guardCells,TargetPFA,Size)

    Noise = cell(10,1); xlen = Size; ylen = Size;
    CA = cell(10,1); GOCA = cell(10,1); SOCA = cell(10,1); OS = cell(10,1);
    DCA = []; DGO = []; DSO = []; DOS = [];
    TCA = cell(10,1); TGO = cell(10,1); TSO = cell(10,1); TOS = cell(10,1);
    
    aCA = getConstant("CA",RefWindow,guardCells,TargetPFA);
    aGO = getConstant("GOCA",RefWindow,guardCells,TargetPFA);
    aSO = getConstant("SOCA",RefWindow,guardCells,TargetPFA);
    aOS = getConstant("OS",RefWindow,0,TargetPFA);
    
    for i = 1:N
       
        Y = randn(ylen,xlen) + 1i*randn(ylen,xlen);
        Data = abs(Y).^2;
        Noise{i} = Data;
        
        TCA{i} = getThreshold("CA",Data,RefWindow,guardCells,aCA);
        TGO{i} = getThreshold("GOCA",Data,RefWindow,guardCells,aGO);
        TSO{i} = getThreshold("SOCA",Data,RefWindow,guardCells,aSO);
        TOS{i} = getThreshold("OS",Data,RefWindow,0,aOS);
        
        [OutCA,DetectionsCA] = getOutput(Data,TCA{i});
        [OutGO,DetectionsGO] = getOutput(Data,TGO{i});
        [OutSO,DetectionsSO] = getOutput(Data,TSO{i});
        [OutOS,DetectionsOS] = getOutput(Data,TOS{i});
        
        DCA(i) = DetectionsCA; DGO(i) = DetectionsGO; 
        DSO(i) = DetectionsSO; DOS(i) = DetectionsOS;
        
        CA{i} = OutCA; GOCA{i} = OutGO; SOCA{i} = OutSO; OS{i} = OutOS;
        
    end

end
%==========================================================================
% END
%==========================================================================
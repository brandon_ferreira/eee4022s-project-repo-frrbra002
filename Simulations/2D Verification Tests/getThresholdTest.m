%==========================================================================
% Author:           Brandon Ferreira
% Student Number:   FRRBRA002 - University of Cape Town
% Title:            getThresholdTest
%
% Description:
%
% This was an attempt to get better results by taking the partial portions
% of the left and right windows in the barrier conditions. The results
% obtained were no different from the current implementation.
%
% Notes:
%   - Currently implemented algorithms include CA, GOCA, SOCA, and OS-CFAR
%
%==========================================================================
function [THRESHOLD] = getThresholdTest(Algorithm,Data,RefWindow,guardCells,alpha)
    %======================================================================
    % Universal Parameters
    %======================================================================
    xlen = size(Data,2);
    ylen = size(Data,1);
    
    barrierOne = (RefWindow/2) + 1;
    barrierTwo = xlen - barrierOne;
    
    N = RefWindow-(2*guardCells);
    k = N/2 + (0);
    %======================================================================
    % Algorithm Selection and Threshold Generation
    %======================================================================
    for y = 1:ylen

        for x = 1:xlen

            leadStart = x - RefWindow/2;
            leadEnd = x - guardCells - 1;
            lagStart = x + guardCells + 1;
            lagEnd = x + RefWindow/2;

            if (x<barrierOne)
                DataLeft = RefWindow/2 - x;
                
                if (Algorithm == "OS")
                    %ghatTemp = sort(cat(2,Data(y:y,lagStart:lagEnd),Data(y:y,lagStart+RefWindow:lagEnd+RefWindow)));
                    ghatTemp = sort(cat(2,Data(y:y,lagStart:lagEnd),Data(y:y,1:x),Data(y:y,lagEnd+1:lagEnd+DataLeft+1)));
                    ghat = ghatTemp(k);
                elseif (Algorithm == "GOCA")
                    ghat = max(sum(Data(y:y,lagStart:lagEnd)),sum(Data(y:y,lagStart+RefWindow:lagEnd+RefWindow)));
                elseif (Algorithm == "SOCA")
                    ghat = min(sum(Data(y:y,lagStart:lagEnd)),sum(Data(y:y,lagStart+RefWindow:lagEnd+RefWindow)));
                elseif (Algorithm == "CA")
                    ghat = sum(Data(y:y,lagStart:lagEnd)) + sum(Data(y:y,lagStart+RefWindow:lagEnd+RefWindow));
                else
                    ghat = 0;
                end
                T(y,x) = alpha*ghat;
                
            elseif (x>barrierTwo)
                DataLeft = xlen - x;
                
                if (Algorithm == "OS")
                    ghatTemp = sort(cat(2,Data(y:y,leadStart:leadEnd),Data(y:y,x:xlen),Data(y:y,leadStart-DataLeft-1:leadStart-1)));
                    ghat = ghatTemp(k);
                elseif (Algorithm == "GOCA")
                    ghat = max(sum(Data(y:y,leadStart:leadEnd)),sum(Data(y:y,leadStart-RefWindow:leadEnd-RefWindow)));
                elseif (Algorithm == "SOCA")
                    ghat = min(sum(Data(y:y,leadStart:leadEnd)),sum(Data(y:y,leadStart-RefWindow:leadEnd-RefWindow)));
                elseif (Algorithm == "CA")
                    ghat = sum(Data(y:y,leadStart:leadEnd)) + sum(Data(y:y,leadStart-RefWindow:leadEnd-RefWindow));
                else
                    ghat = 0;
                end
                T(y,x) = alpha*ghat;
                
            else
                
                if (Algorithm == "OS")
                    ghatTemp = sort(cat(2,Data(y:y,leadStart:leadEnd),Data(y:y,lagStart:lagEnd)));
                    ghat = ghatTemp(k);
                elseif (Algorithm == "GOCA")
                    ghat = max(sum(Data(y:y,leadStart:leadEnd)),sum(Data(y:y,lagStart:lagEnd)));
                elseif (Algorithm == "SOCA")
                    ghat = min(sum(Data(y:y,leadStart:leadEnd)),sum(Data(y:y,lagStart:lagEnd)));
                elseif (Algorithm == "CA")
                    ghat = sum(Data(y:y,leadStart:leadEnd)) + sum(Data(y:y,lagStart:lagEnd));
                else

                end
                T(y,x) = alpha*ghat;
                
            end
        end
    end

    THRESHOLD = T;
    
end
%==========================================================================
% END
%==========================================================================
%==========================================================================
% Author:           Brandon Ferreira
% Student Number:   FRRBRA002 - University of Cape Town
% Title:            Verify_2D
%
% Description:
%
% Generates noise samples and applies thresholding techniques on all.
% Returns the thresholded data as well as originally generated noise. The
% purpose is to mass test the False alarm rate for different detection
% algorthims.
%
% Notes:
%   - If the PFA error is under 10% the algorithm is working as expected.
%==========================================================================
clear all;
close all;
%==========================================================================
% Parameter Setup
%==========================================================================
RefWindow = 20;  
guardCells = 1;
TargetPFA = 10^-3;
DataSize = 1000;
%==========================================================================
% Results and Calculation
%==========================================================================
N = RefWindow - (2*guardCells);
[Noise,CA,DCA,TCA,GOCA,DGO,TGO,SOCA,DSO,TSO,OS,DOS,TOS] = getNoiseSamples(2,RefWindow,guardCells,TargetPFA,DataSize);
%==========================================================================
for i = 1:2
    figure();
    
    imagesc(1:DataSize,1:DataSize,Noise{i});
    title("Noise Data");xlabel('Row Data');ylabel('Column Data');
    colorbar();

    figure();
    
    subplot(2,2,1);imagesc(1:DataSize,1:DataSize,TCA{i});
    title("Threshold - CA");xlabel('Row Data');ylabel('Column Data');
    set(gca,'FontSize',17);colormap(jet(256));colorbar;caxis([-15 50]-3);
    
    subplot(2,2,2);imagesc(1:DataSize,1:DataSize,TGO{i});
    title("Threshold - GOCA");xlabel('Row Data');ylabel('Column Data');
    set(gca,'FontSize',17);colormap(jet(256));colorbar;caxis([-15 50]-3);
    
    subplot(2,2,3);imagesc(1:DataSize,1:DataSize,TSO{i});
    title("Threshold - SOCA");xlabel('Row Data');ylabel('Column Data');
    set(gca,'FontSize',17);colormap(jet(256));colorbar;caxis([-15 50]-3);
    
    subplot(2,2,4);imagesc(1:DataSize,1:DataSize,TOS{i});
    title("Threshold - OS");xlabel('Row Data');ylabel('Column Data');
    set(gca,'FontSize',17);colormap(jet(256));colorbar;caxis([-15 50]-3);
    
    figure();
    
    subplot(2,2,1);imagesc(1:DataSize,1:DataSize,CA{i});
    title("Output Data - CA");xlabel('Row Data');ylabel('Column Data');
    subplot(2,2,2);imagesc(1:DataSize,1:DataSize,GOCA{i});
    title("Output Data - GOCA");xlabel('Row Data');ylabel('Column Data');
    subplot(2,2,3);imagesc(1:DataSize,1:DataSize,SOCA{i});
    title("Output Data - SOCA");xlabel('Row Data');ylabel('Column Data');
    subplot(2,2,4);imagesc(1:DataSize,1:DataSize,OS{i});
    title("Output Data - OS");xlabel('Row Data');ylabel('Column Data');
end

display(DCA);display(DGO);display(DSO);display(DOS);
%==========================================================================
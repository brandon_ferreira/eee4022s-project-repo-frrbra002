%==========================================================================
% Author:           Brandon Ferreira
% Student Number:   FRRBRA002 - University of Cape Town
% Title:            Verify_1D
%
% Description:
%
% getConstant will return the CFAR constant of an algorithm specified
% through given input parameters
%
% Notes:
%   - Currently implemented algorithms include CA, GOCA, SOCA, and OS-CFAR
%
%==========================================================================
close all;
clear all;

% Data Setup
%==========================================================================4

%==========================================================================
% CFAR Parameter Setup
%==========================================================================

%==========================================================================
% CFAR Constants
%==========================================================================
RefWindow = 32;
guardCells = 1;
N = RefWindow - (2*guardCells);
TargetPFA = 10^-3;

aCA = getConstant("CA",RefWindow,guardCells,TargetPFA);
aGO = getConstant("GOCA",RefWindow,guardCells,TargetPFA);
aSO = getConstant("SOCA",RefWindow,guardCells,TargetPFA);
aOS = getConstant("OS",RefWindow,guardCells,TargetPFA);
%==========================================================================
% Test 1 - Size = 10000
%==========================================================================
FA_1_CA = [];FA_1_GO = [];FA_1_SO = [];FA_1_OS = [];
DataSize = 10000;
for i = 1:10
    
    Data = randn(1,DataSize) + 1i*randn(1,DataSize);
    Data = abs(Data).^2;
    
    TCA = getThreshold("CA",Data,RefWindow,guardCells,aCA);
    TGO = getThreshold("GOCA",Data,RefWindow,guardCells,aGO);
    TSO = getThreshold("SOCA",Data,RefWindow,guardCells,aSO);
    TOS = getThreshold("OS",Data,RefWindow,guardCells,aOS);
    
    [OutputCA,DetectionsCA,xDetectCA,yDetectCA] = getOutput(Data,TCA);
    [OutputGO,DetectionsGO,xDetectGO,yDetectGO] = getOutput(Data,TGO);
    [OutputSO,DetectionsSO,xDetectSO,yDetectSO] = getOutput(Data,TSO);
    [OutputOS,DetectionsOS,xDetectOS,yDetectOS] = getOutput(Data,TOS);
    
    FA_1_CA(i) = DetectionsCA;
    FA_1_GO(i) = DetectionsGO;
    FA_1_SO(i) = DetectionsSO;
    FA_1_OS(i) = DetectionsOS;
end
%==========================================================================
% Test 2 - Size = 100000
%==========================================================================
FA_2_CA = [];FA_2_GO = [];FA_2_SO = [];FA_2_OS = [];
DataSize = 100000;
for i = 1:10
    
    Data = randn(1,DataSize) + 1i*randn(1,DataSize);
    Data = abs(Data).^2;
    
    TCA = getThreshold("CA",Data,RefWindow,guardCells,aCA);
    TGO = getThreshold("GOCA",Data,RefWindow,guardCells,aGO);
    TSO = getThreshold("SOCA",Data,RefWindow,guardCells,aSO);
    TOS = getThreshold("OS",Data,RefWindow,guardCells,aOS);
    
    [OutputCA,DetectionsCA,xDetectCA,yDetectCA] = getOutput(Data,TCA);
    [OutputGO,DetectionsGO,xDetectGO,yDetectGO] = getOutput(Data,TGO);
    [OutputSO,DetectionsSO,xDetectSO,yDetectSO] = getOutput(Data,TSO);
    [OutputOS,DetectionsOS,xDetectOS,yDetectOS] = getOutput(Data,TOS);
    
    FA_2_CA(i) = DetectionsCA;
    FA_2_GO(i) = DetectionsGO;
    FA_2_SO(i) = DetectionsSO;
    FA_2_OS(i) = DetectionsOS;
end
%==========================================================================
% Test 3 - Size = 1000000
%==========================================================================
DataSize = 1000000;
for i = 1:10
    
    Data = randn(1,DataSize) + 1i*randn(1,DataSize);
    Data = abs(Data).^2;
    
    TCA = getThreshold("CA",Data,RefWindow,guardCells,aCA);
    TGO = getThreshold("GOCA",Data,RefWindow,guardCells,aGO);
    TSO = getThreshold("SOCA",Data,RefWindow,guardCells,aSO);
    TOS = getThreshold("OS",Data,RefWindow,guardCells,aOS);
    
    [OutputCA,DetectionsCA,xDetectCA,yDetectCA] = getOutput(Data,TCA);
    [OutputGO,DetectionsGO,xDetectGO,yDetectGO] = getOutput(Data,TGO);
    [OutputSO,DetectionsSO,xDetectSO,yDetectSO] = getOutput(Data,TSO);
    [OutputOS,DetectionsOS,xDetectOS,yDetectOS] = getOutput(Data,TOS);
    
    FA_3_CA(i) = DetectionsCA;
    FA_3_GO(i) = DetectionsGO;
    FA_3_SO(i) = DetectionsSO;
    FA_3_OS(i) = DetectionsOS;
end
%==========================================================================
% END
%==========================================================================

%==========================================================================
% Author:           Brandon Ferreira
% Student Number:   FRRBRA002 - University of Cape Town
% Title:            Verify_1D
%
% Description:
%
% getConstant will return the CFAR constant of an algorithm specified
% through given input parameters
%
% Notes:
%   - Currently implemented algorithms include CA, GOCA, SOCA, and OS-CFAR
%
%==========================================================================
close all;
clear all;
%==========================================================================
% Data Setup
%==========================================================================4
DataSize = 300;
Data = randn(1,DataSize) + 1i*randn(1,DataSize);

%Data(1,35:40) = Data(1,35:40).*10;
Data = abs(Data).^2;
%==========================================================================
% CFAR Parameter Setup
%==========================================================================
RefWindow = 32;
guardCells = 1;

N = RefWindow - (2*guardCells);
TargetPFA = 10^-3;
%==========================================================================
% CFAR Constants
%==========================================================================
aCA = getConstant("CA",RefWindow,guardCells,TargetPFA);
aGO = getConstant("GOCA",RefWindow,guardCells,TargetPFA);
aSO = getConstant("SOCA",RefWindow,guardCells,TargetPFA);
aOS = getConstant("OS",RefWindow,guardCells,TargetPFA);
%==========================================================================
% Thresholds
%==========================================================================
TCA = getThreshold("CA",Data,RefWindow,guardCells,aCA);
TGO = getThreshold("GOCA",Data,RefWindow,guardCells,aGO);
TSO = getThreshold("SOCA",Data,RefWindow,guardCells,aSO);
TOS = getThreshold("OS",Data,RefWindow,guardCells,aOS);
%==========================================================================
% Outputs
%==========================================================================
[OutputCA,DetectionsCA,xDetectCA,yDetectCA] = getOutput(Data,TCA);
[OutputGO,DetectionsGO,xDetectGO,yDetectGO] = getOutput(Data,TGO);
[OutputSO,DetectionsSO,xDetectSO,yDetectSO] = getOutput(Data,TSO);
[OutputOS,DetectionsOS,xDetectOS,yDetectOS] = getOutput(Data,TOS);
%==========================================================================
% Plots
%==========================================================================
TargetDetections = (DataSize)*TargetPFA;
PFA_ErrorCA = ((abs(DetectionsCA - TargetDetections))/TargetDetections)*100;
PFA_ErrorGO = ((abs(DetectionsGO - TargetDetections))/TargetDetections)*100;
PFA_ErrorSO = ((abs(DetectionsSO - TargetDetections))/TargetDetections)*100;
PFA_ErrorOS = ((abs(DetectionsOS - TargetDetections))/TargetDetections)*100;

%disp(DataSize);
%T = table([DataSize;RefWindow;guardCells;TargetDetections;"<= 10%"],[DataSize;RefWindow;guardCells;DetectionsCA;PFA_ErrorCA],[DataSize;RefWindow;guardCells;DetectionsGO;PFA_ErrorGO],[DataSize;RefWindow;guardCells;DetectionsSO;PFA_ErrorSO],[DataSize;RefWindow;guardCells;DetectionsOS;PFA_ErrorOS],'VariableNames',{'Target', 'CA','GOCA','SOCA','OS'},'RowName',{'Data Size','Reference Window Size','GuardCells','FA','PFA Error'});
%disp(T);

figure();
%plot(1:DataSize,Data,1:DataSize,TCA,1:DataSize,TGO,1:DataSize,TSO,1:DataSize,TOS); 
plot(1:DataSize,20*log10(Data),1:DataSize,20*log10(TCA),1:DataSize,20*log10(TGO),1:DataSize,20*log10(TSO),1:DataSize,20*log10(TOS));
title("Noise and Data Thresholds"); 
xlabel("Samples"); 
ylabel("Magnitude");
legend("Noise","CA-CFAR","GOCA-CFAR","SOCA-CFAR","OS-CFAR");


%{
figure();

subplot(2,2,1); plot(1:DataSize,Data); title("Original Noise Data"); xlabel("Samples"); ylabel("Magnitude");
subplot(2,2,2); plot(1:DataSize,TGO); title("Threshold - GOCA"); xlabel("Samples"); ylabel("Magnitude");
subplot(2,2,3); plot(1:DataSize,Data,1:DataSize,TGO); title("Noise and Threshold - GOCA"); xlabel("Samples"); ylabel("Magnitude");
subplot(2,2,4); plot(1:DataSize,OutputGO); title("Noise Data with non-detections set to zero - GOCA"); xlabel("Samples"); ylabel("Magnitude");

figure();

subplot(2,2,1); plot(1:DataSize,Data); title("Original Noise Data"); xlabel("Samples"); ylabel("Magnitude");
subplot(2,2,2); plot(1:DataSize,TSO); title("Threshold - SOCA"); xlabel("Samples"); ylabel("Magnitude");
subplot(2,2,3); plot(1:DataSize,Data,1:DataSize,TSO); title("Noise and Threshold - SOCA"); xlabel("Samples"); ylabel("Magnitude");
subplot(2,2,4); plot(1:DataSize,OutputSO); title("Noise Data with non-detections set to zero - SOCA"); xlabel("Samples"); ylabel("Magnitude");

figure();

subplot(2,2,1); plot(1:DataSize,Data); title("Original Noise Data"); xlabel("Samples"); ylabel("Magnitude");
subplot(2,2,2); plot(1:DataSize,TOS); title("Threshold - OS"); xlabel("Samples"); ylabel("Magnitude");
subplot(2,2,3); plot(1:DataSize,Data,1:DataSize,TOS); title("Noise and Threshold - OS"); xlabel("Samples"); ylabel("Magnitude");
subplot(2,2,4); plot(1:DataSize,OutputOS); title("Noise Data with non-detections set to zero - OS"); xlabel("Samples"); ylabel("Magnitude");
%}

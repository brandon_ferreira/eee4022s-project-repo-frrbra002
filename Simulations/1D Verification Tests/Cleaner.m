close all; clear all;

load NewData_R64G3;

FA_1_CA = FA_1_CA.';
FA_1_GO = FA_1_GO.';
FA_1_SO = FA_1_SO.';
FA_1_OS = FA_1_OS.';
final1 = [FA_1_CA FA_1_GO FA_1_SO FA_1_OS];

FA_2_CA = FA_2_CA.';
FA_2_GO = FA_2_GO.';
FA_2_SO = FA_2_SO.';
FA_2_OS = FA_2_OS.';
final2 = [FA_2_CA FA_2_GO FA_2_SO FA_2_OS];

FA_3_CA = FA_3_CA.';
FA_3_GO = FA_3_GO.';
FA_3_SO = FA_3_SO.';
FA_3_OS = FA_3_OS.';
final3= [FA_3_CA FA_3_GO FA_3_SO FA_3_OS];
%}
%==========================================================================
% Author:           Brandon Ferreira
% Student Number:   FRRBRA002 - University of Cape Town
% Title:            getStatistic
%
% Description:
%
% getStatistic will return the CFAR statistic of an algorithm specified
% through given input parameters
%
% Notes:
%   - Currently implemented algorithms include CA, GOCA, SOCA, and OS-CFAR
%
%==========================================================================
function [THRESHOLD] = getThreshold(Algorithm,Data,RefWindow,guardCells,alpha)
    %======================================================================
    % Universal Parameters
    %======================================================================
    xlen = size(Data,2);
    ylen = size(Data,1);
    
    barrierOne = (RefWindow/2) + 1;
    barrierTwo = xlen - barrierOne;
    
    N = RefWindow-(2*guardCells);
    %k = N/2 + (0);
    k = round(0.75*N);
    %======================================================================
    % Algorithm Selection and Threshold Generation
    %======================================================================
    for y = 1:ylen

        for x = 1:xlen

            leadStart = x - RefWindow/2;
            leadEnd = x - guardCells - 1;
            lagStart = x + guardCells + 1;
            lagEnd = x + RefWindow/2;

            if (x<barrierOne)
                
                if (Algorithm == "OS")
                    %ghatTemp = sort(cat(2,Data(y:y,lagStart:lagEnd),Data(y:y,lagStart:lagEnd)));
                    ghatTemp = sort(cat(2,Data(y:y,lagStart:lagEnd),Data(y:y,lagStart+RefWindow/2:lagEnd+RefWindow/2)));
                    ghat = ghatTemp(k);
                elseif (Algorithm == "GOCA")
                    %ghat = sum(Data(y:y,lagStart:lagEnd));
                    ghat = max(sum(Data(y:y,lagStart:lagEnd)),sum(Data(y:y,lagStart+RefWindow/2:lagEnd+RefWindow/2)));
                elseif (Algorithm == "SOCA")
                    %ghat = sum(Data(y:y,lagStart:lagEnd));
                    ghat = min(sum(Data(y:y,lagStart:lagEnd)),sum(Data(y:y,lagStart+RefWindow/2:lagEnd+RefWindow/2)));
                elseif (Algorithm == "CA")
                    %ghat = 2*sum(Data(y:y,lagStart:lagEnd));
                    ghat = sum(Data(y:y,lagStart:lagEnd)) + sum(Data(y:y,lagStart+RefWindow/2:lagEnd+RefWindow/2));
                else
                    ghat = 0;
                end
                T(y,x) = alpha*ghat;
                
            elseif (x>barrierTwo)
                
                if (Algorithm == "OS")
                    %ghatTemp = sort(cat(2,Data(y:y,leadStart:leadEnd),Data(y:y,leadStart:leadEnd)));
                    ghatTemp = sort(cat(2,Data(y:y,leadStart:leadEnd),Data(y:y,leadStart-RefWindow/2:leadEnd-RefWindow/2)));
                    ghat = ghatTemp(k);
                elseif (Algorithm == "GOCA")
                    %ghat = sum(Data(y:y,leadStart:leadEnd));
                    ghat = max(sum(Data(y:y,leadStart:leadEnd)),sum(Data(y:y,leadStart-RefWindow/2:leadEnd-RefWindow/2)));
                elseif (Algorithm == "SOCA")
                    %ghat = sum(Data(y:y,leadStart:leadEnd));
                    ghat = min(sum(Data(y:y,leadStart:leadEnd)),sum(Data(y:y,leadStart-RefWindow/2:leadEnd-RefWindow/2)));
                elseif (Algorithm == "CA")
                    %ghat = 2*sum(Data(y:y,leadStart:leadEnd));
                    ghat = sum(Data(y:y,leadStart:leadEnd)) + sum(Data(y:y,leadStart-RefWindow/2:leadEnd-RefWindow/2));
                else
                    ghat = 0;
                end
                T(y,x) = alpha*ghat;
                
            else
                
                if (Algorithm == "OS")
                    ghatTemp = sort(cat(2,Data(y:y,leadStart:leadEnd),Data(y:y,lagStart:lagEnd)));
                    ghat = ghatTemp(k);
                elseif (Algorithm == "GOCA")
                    ghat = max(sum(Data(y:y,leadStart:leadEnd)),sum(Data(y:y,lagStart:lagEnd)));
                elseif (Algorithm == "SOCA")
                    ghat = min(sum(Data(y:y,leadStart:leadEnd)),sum(Data(y:y,lagStart:lagEnd)));
                elseif (Algorithm == "CA")
                    ghat = sum(Data(y:y,leadStart:leadEnd)) + sum(Data(y:y,lagStart:lagEnd));
                else
                    ghat = 0;
                end
                T(y,x) = alpha*ghat;
                
            end
        end
    end

    THRESHOLD = T;
    
end
%==========================================================================
% END
%==========================================================================
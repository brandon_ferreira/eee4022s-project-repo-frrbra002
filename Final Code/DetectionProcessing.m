%==========================================================================
% Author:           Brandon Ferreira
% Student Number:   FRRBRA002 - University of Cape Town
% Title:            DetectionProcessing
%
% Description:
%
% Interface for accessing the various parts of the radar detection process
% such as getConstants() and getThreshold. Produces the detections marked
% on a RTI map for both straigh-RTI processing and Range-Doppler Processing
%
% Notes:
%   - Data from loaded data sets are in a matrix called sif
%   - guardCell Parameter only applies to CA, GOCA, and SOCA algorithms
%==========================================================================
% Preliminary Operations and Data Loading
%==========================================================================
clear all;
close all;
%==========================================================================
% Available data sets
%
% Need to comment out all that are not in use
%
% Note: The same process needs to be performed in generatePlots or the
% detections will not map correctly
%==========================================================================
%load TwoPeopleWalking_Outdoors.mat;
%load TwoPeopleWalking_Indoors.mat;
load PersonWalking_CarLater_Outdoor.mat;
%load OnePersonWalking_Indoors.mat;
%load CarOutdoors.mat;
%load 2CarsAndB.mat;
%load OneCar.mat;
%==========================================================================
% Parameters - User Editable
%==========================================================================
% getFrames Input Parameters
windowSize = 16;

% getConstant Input Parameters
RefWindow = 36;
guardCells = 3;
guardCellsOS = 0;
TargetPFA = 10^-5;

% Test selection
DataType = 1;

% CFAR Constants
aCA = getConstant("CA",RefWindow,guardCells,TargetPFA);
aGO = getConstant("GOCA",RefWindow,guardCells,TargetPFA);
aSO = getConstant("SOCA",RefWindow,guardCells,TargetPFA);
aOS = getConstant("OS",RefWindow,guardCellsOS,TargetPFA);
%==========================================================================
% Function Available and Parameters Required
%==========================================================================
% [dataFrames,RDMaps] = getFrames(sif,windowSize);
% [alpha] = getConstant("Algorithm",RefWindow,guardCells,TargetPFA);
% [T] = getThreshold("Algorithm",abs(sif).^2,RefWindow,guardCells,alpha);
% [Output,Detections] = getOutput(abs(sif).^2,T);4
% [Noise,CA,DCA,GOCA,DGO,SOCA,DSO,OS,DOS] = getNoiseSamples(N,RefWindow,guardCells,TargetPFA)
%
% generatePlots(Data1,Data2,SampleCount,D1_Title,D2_Title)
%==========================================================================
% Noise Simulations
%==========================================================================
if (DataType == 0)
    
    SampleCount = 10;
    %----------------------------------------------------------------------
    % Data Variable Gathering
    %----------------------------------------------------------------------
    [NoiseData,CA_Data,DCA,GOCA_Data,DGO,SOCA_Data,DSO,OS_Data,DOS] = getNoiseSamples(SampleCount,RefWindow,guardCells,TargetPFA);
    %----------------------------------------------------------------------
    generatePlots(NoiseData,CA_Data,SampleCount,"Noise Data","CA Thresholding",0,0,0);
    generatePlots(NoiseData,GOCA_Data,SampleCount,"Noise Data","GOCA Thresholding",0,0,0);
    generatePlots(NoiseData,SOCA_Data,SampleCount,"Noise Data","SOCA Thresholding",0,0,0);
    generatePlots(NoiseData,OS_Data,SampleCount,"Noise Data","OS Thresholding",0,0,0);
    
    display(DCA);display(DGO);display(DSO);display(DOS);
    
%==========================================================================
% Provided Data Set Tests
%==========================================================================
elseif (DataType == 1)
    % Select Data
    Data = sif;
    Data_SL = abs(sif).^2;
    [dataFrames,RDMaps] = getFrames(Data,windowSize);    
    % Detection on frames
    frameCount = size(dataFrames,1);
    frameSize = 16;
    yDetectFinal = [];
    xDetectFinal = [];
    finalIndex = 1;
    
    for i = 1:frameCount

        RD_Data = abs(RDMaps{i}).^2;
        T = getThreshold("CA",RD_Data,RefWindow,guardCells,aCA);
        [Out,DC,xDetect,yDetect] = getOutput(RD_Data,T);
        
        for z = 1:DC
            yDetectFinal(finalIndex) = yDetect(z) + i*8;
            xDetectFinal(finalIndex) = xDetect(z);
            finalIndex = finalIndex + 1;
        end     
        %generatePlots(RD_Data,Out,1,"Range-Doppler Map","Range-Doppler Map Detections",1,xDetect,yDetect);
    end 
    generatePlots(20*log10(Data_SL),20*log10(Data_SL),1,"Original Data RTI - 2 Cars and a Bike - CA-CFAR","Detections - Range-Doppler Processing - CA-CFAR",1,xDetectFinal,yDetectFinal);
    
    T_main = getThreshold("CA",Data_SL,RefWindow,guardCells,aCA);
    [Out_main,DC,xDetect,yDetect] = getOutput(Data_SL,T_main);
    generatePlots(20*log10(Data_SL),20*log10(Data_SL),1,"Original Data - 2 Cars and a Bike - CA-CFAR","Detections - RTI Data - CA-CFAR",1,xDetect,yDetect);
    
    %{
    figure();
    imagesc(dataRange,(1:numPulses)*Tp*2,20*log10(Data_SL));
    xlabel('Range (m)'); ylabel('Time (s)');
    title("Original Sample Data");
    %}
%==========================================================================
% Measured Data Set Tests
%==========================================================================
else
    
end
%==========================================================================
%  Window Functions
%==========================================================================
function [w] = hann_window(N)
    % create a hann (cosine squared) window
    w = .5 + .5*cos(2*pi*((1:N).'/(N+1) - .5));
end
%==========================================================================
%  End
%==========================================================================
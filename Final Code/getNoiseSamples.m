%==========================================================================
% Author:           Brandon Ferreira
% Student Number:   FRRBRA002 - University of Cape Town
% Title:            getNoiseSamples
%
% Description:
%
% Generates noise samples and applies thresholding techniques on all.
% Returns the thresholded data as well as originally generated noise.
%
% Notes:
%
%==========================================================================
function [Noise,CA,DCA,GOCA,DGO,SOCA,DSO,OS,DOS] = getNoiseSamples(N,RefWindow,guardCells,TargetPFA)

    Noise = cell(10,1); xlen = 1000; ylen = 100;
    CA = cell(10,1); GOCA = cell(10,1); SOCA = cell(10,1); OS = cell(10,1);
    DCA = []; DGO = []; DSO = []; DOS = [];
    
    aCA = getConstant("CA",RefWindow,guardCells,TargetPFA);
    aGO = getConstant("GOCA",RefWindow,guardCells,TargetPFA);
    aSO = getConstant("SOCA",RefWindow,guardCells,TargetPFA);
    aOS = getConstant("OS",RefWindow,0,TargetPFA);
    
    for i = 1:N
       
        Y = randn(ylen,xlen) + 1i*randn(ylen,xlen);
        Data = abs(Y).^2;
        Noise{i} = Data;
        
        TCA = getThreshold("CA",Data,RefWindow,guardCells,aCA);
        TGO = getThreshold("GOCA",Data,RefWindow,guardCells,aGO);
        TSO = getThreshold("SOCA",Data,RefWindow,guardCells,aSO);
        TOS = getThreshold("OS",Data,RefWindow,0,aOS);
        
        [OutCA,DetectionsCA] = getOutput(Data,TCA);
        [OutGO,DetectionsGO] = getOutput(Data,TGO);
        [OutSO,DetectionsSO] = getOutput(Data,TSO);
        [OutOS,DetectionsOS] = getOutput(Data,TOS);
        
        DCA(i) = DetectionsCA; DGO(i) = DetectionsGO; 
        DSO(i) = DetectionsSO; DOS(i) = DetectionsOS;
        
        CA{i} = OutCA; GOCA{i} = OutGO; SOCA{i} = OutSO; OS{i} = OutOS;
        
    end

end
%==========================================================================
% END
%==========================================================================
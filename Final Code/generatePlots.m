%==========================================================================
% Author:           Brandon Ferreira
% Student Number:   FRRBRA002 - University of Cape Town
% Title:            generatePlots
%
% Description:
%
% Takes in data matrices and generates figures using matlab imagesc
%
% Notes:
%
%==========================================================================
function generatePlots(Data1,Data2,SampleCount,D1_Title,D2_Title,Type,xDetect,yDetect)

    if (Type == 0)
        
        for i = 1:SampleCount
            figure();

            subplot(2,1,1);
            imagesc(Data1{i});
            title(D1_Title);

            subplot(2,1,2);
            imagesc(Data2{i});
            title(D2_Title);
        end
    
    elseif (Type == 1)
        
        %load TwoPeopleWalking_Outdoors.mat;
        %load TwoPeopleWalking_Indoors.mat;
        load PersonWalking_CarLater_Outdoor.mat;
        %load OnePersonWalking_Indoors.mat;
        %load CarOutdoors.mat;
        %load 2CarsAndB.mat
        %load OneCar.mat;
    
        yDetect = yDetect.*(2*Tp);
        xDetect = xDetect.*(100/108);
        
        figure('Renderer', 'painters', 'Position', [10 10 1000 700]);

        %subplot(2,1,1);
        %imagesc(dataRange,(1:numPulses)*Tp*2,Data1);
        %xlabel('Range (m)'); ylabel('Time (s)');
        %title(D1_Title);

        %subplot(2,1,2);
        %imagesc(Data2);
        imagesc(dataRange,(1:numPulses)*Tp*2,Data2);
        xlabel('Range (m)','fontsize',14); ylabel('Time (s)','fontsize',14);
        title(D2_Title);
        colormap(jet(256));
        caxis([-50 120]-3);
        hold on;
        plot(xDetect,yDetect,'kx','MarkerSize',8,'LineWidth',2);
        %}

    else
        
    end
    
end
%==========================================================================
% END
%==========================================================================
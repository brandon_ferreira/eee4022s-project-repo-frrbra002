%==========================================================================
% Author:           Brandon Ferreira
% Student Number:   FRRBRA002 - University of Cape Town
% Title:            getFrames
%
% Description:
%
% getFrames accepts a data matrix (i.e: RTI map) and splits the matrix into
% smaller frames for later processing such as Range-Doppler map generation
%
% Notes:
%   - Makes use of a overlap factor of 50%
%
%==========================================================================
function [frames,RDs] = getFrames(data,frameSize)

    frames = cell(10,1);
    RDs = cell(10,1);
    
    ylen = size(data,1);
    
    startIndex = 1;
    endIndex = frameSize;
    dataIndex = 1;
    
    while(1)
        tempMatrix = data(startIndex:endIndex,:);
        frames{dataIndex} = tempMatrix;
        RDs{dataIndex} = fftshift(fft(tempMatrix.*hann_window(frameSize),[],1),1);
        % Index Updating
        startIndex = startIndex + frameSize/2;
        endIndex = endIndex + frameSize/2;
        dataIndex = dataIndex + 1;
        % End Condition Check
        if (endIndex>ylen)
           break; 
        end
    end
    
end
%==========================================================================
%  Window Functions
%==========================================================================
function [w] = hann_window(N)
    % create a hann (cosine squared) window
    w = .5 + .5*cos(2*pi*((1:N).'/(N+1) - .5));
end
%==========================================================================
% End
%==========================================================================